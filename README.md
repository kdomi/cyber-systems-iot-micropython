 IoT Microcontroller written in Micropython
=============================================

About
----------

These programs were written for the 4th Assignment of the course Cyber Systems at the Technical University of Denmark.
The members of the team:
- Sammy Benomar
- Antonio Iglesias
- Kristóf Wesely
- Dominik Kovács

Setup
----------
For the project an ESP32 Feather Huzzah was used, with the following configuration:
- 3 Neopixel LEDS
- 1 Temperature Sensor

The neopixels are controlled by Pin 13. The setup is seen in the **main.py:**

```
np = neopixel.NeoPixel(machine.Pin(13),3)
i2c = machine.I2C(scl=machine.Pin(22), sda=machine.Pin(23))
```

The **main.py** and the **temp_cal.py** files must be placed on the pyboard. The rest of the files are run on the computer.

Website
-------------------------
By going to the IP address: **http://192.168.4.1** we can see the current state of the board. It automatically updates the temperature by using http get requests. This was task 1-3.

Logging the temperature
--------------------------
By running the **log.py** file we are able to log the time and the temperature in celsius what we get from the pyboard. This was task 4.
Run it like:
```
python log.py <path for the text file>
```

Interactive Environment
--------------------------
By running the **control.py** file we can use the interactive environment to remotely control the pyboard, this was task 5. It includes features like:
- Printing the current temperature
- Changing the color of specific Neopixel LEDs
- Turning off all the LEDs
- Asking for the current color of the LEDs in RGB

Licensing
-------------
Copyright 2018 Sammy Benomar, Antonio Iglesias, Kristóf Wesely and Dominik Kovács

This is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with it.  If not, see https://www.gnu.org/licenses/.