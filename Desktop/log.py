#Task 4
import requests
import sys
import time
import datetime
import temp_get

time_now = datetime.datetime.now

def log_data(path):
    with open(path, "w") as f:
        try:
            while True:
                f.write("{} {}\n".format(time_now(),temp_get.main()))
                print("{} {} Celsius\n".format(time_now(),temp_get.main()))
                time.sleep(1)
        except KeyboardInterrupt:
            print("Interrupted, quitting the program...")
        
        
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Too few arguments. Please input python task4.py <path for the file>")
        exit()
    path = sys.argv[1]
    log_data(path)
        