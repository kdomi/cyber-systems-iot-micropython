#Task 5
#imports
import requests
import temp_get

print("\nWelcome to the ESP32 Feather Huzzah interactive environment")

def configure_led():
    r = requests.get('http://192.168.4.1/neopixel/get')
    print("\n1. LED: {}\n2. LED: {}\n3. LED: {}".format(r.json()[0],r.json()[1],r.json()[2]))
    print("Which LED would you like to configure?")
    while True:
        try:
            choice = int(input("\nYour command: "))
            if choice in [1,2,3]:
                print("\nPlease give the RBG value for LED {} in the following format:\nRED-GREEN-BLUE".format(choice))
                while True:
                    try:
                        color = input("\nYour color: ")
                        if color == "0":
                            configure_led()
                        color = [int(x) for x in color.split("-")]
                        if len(color) == 3:
                            if False in [i in range(256) for i in color]:
                                print("Please make sure that your RGB values are between 0 and 255")
                            else:
                                print("Changing LED color...")
                                r = requests.get('http://192.168.4.1/neopixel/change/{}/{},{},{}'.format(choice-1,*color))
                                print("LED color changed")
                                configure_led()
                        else:
                            print("Please follow the format and make sure to use integers, e.g.: 255-56-40 or give 0 to go back")
                    except ValueError:
                        print("Please follow the format, e.g.: 255-56-40 or give 0 to go back")
            elif choice == 0:
                led()
            else:
             print("Please give either 1, 2, 3 or 0 to go back")
        except ValueError:
            print("Please give either 1, 2, 3 or 0 to go back")
    print(r.json()[0][0])

def led():
    print("\n0.: Back to the main menu\n1.: Configure the LEDs specifically\n2.: Turn off all LEDs")
    while True:
        try:
            choice = int(input("\nYour command: "))
            if choice == 0:
                print("Returning to main menu...")
                main()
            elif choice == 1:
                print("\nLoading LED configurations in RGB..")
                configure_led()
            elif choice == 2:
                print("\nTurning of all Leds...")
                r = requests.get('http://192.168.4.1/neopixel/off')
                print("All LEDs are turned off")
            else:
                print("Please give one of the given numbers")
        except ValueError:
            print("Please give an integer")


def main():
    print("\n0.: Log the temperature data\n1.: LED configuations\n2.: Exit program")
    while True:
        try:
            choice = int(input("\nYour command: "))
            if choice == 0:
                print("\nLoading temperature...")
                print("Current temperature is {} Celsius".format(temp_get.main()))
                main()
            elif choice == 1:
                print("\nLoading LED options...")
                led()
            elif choice == 2:
                print("\nQuitting program...")
                exit()
            else:
                print("Please give one of the given numbers")
            
        except ValueError:
            print("Please give an integer")    

#Initialize the program            
main()