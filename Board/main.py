# Main file for the pyboard
import machine
import network
import socket
import neopixel
import temp_cal
import urequests
import json
#Headers
h200_json = "HTTP/1.1 200 OK\nContent-Type: application/json\n\n"
#Network code part
ap = network.WLAN (network.AP_IF)
ap.active (True)
ap.config (essid = '4manteamlol')
ap.config (authmode = 3, password = 'ihavethehighground')

pins = [machine.Pin(i, machine.Pin.IN) for i in (0, 2, 4, 5, 12, 13, 14, 15)]
# Initialize neopixel
np = neopixel.NeoPixel(machine.Pin(13),3)
# Making sure the leds are off
for i in range(3):
    np[i] = (0,0,0)
np.write()

html = """<!DOCTYPE html>
<html>
    <head>
        <title>ESP32 Pins</title>
    <style>
        body{
            background-color: black;
            color: white;
            font-family: sans-serif;
        }
        .title{
            text-align: center;
        }
        table{
            margin: 0px auto;
        }

    </style>
    </head>
    <body>
        <h1 class="title">ESP32 Feather Huzzah</h1>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
     
    <script>
   
        // request data updates every 2500 milliseconds
        setInterval(requestDataSensor, 2500);
        setInterval(requestDataPins, 9000);
        
        function requestDataSensor() {  
          
          var xhr = new XMLHttpRequest();
          xhr.open('GET', 'sensors');

          xhr.onload = function() {
            if (xhr.status === 200) {

              if (xhr.responseText) { // if the returned data is not null, update the values

                var data = JSON.parse(xhr.responseText);
                document.getElementById("temp").innerText = data;

              } else { // a problem occurred

                console.log("A problem occured")
              }
            } else {
              console.log('Request failed for sensors.  Returned status of ' + xhr.status);
            }
          };
          xhr.send();
        }
        
        function requestDataPins(){
          var xhr = new XMLHttpRequest();
          xhr.open('GET', 'pins');

          xhr.onload = function() {
            if (xhr.status === 200) {

              if (xhr.responseText) { // if the returned data is not null, update the values

                var data = JSON.parse(xhr.responseText);

                console.log(data);

              } else { // a problem occurred

                console.log("A problem occured")
              }
            } else {
              console.log('Request failed for pins.  Returned status of ' + xhr.status);
            }
          };
          xhr.send();
        }
    </script>
    </body>
</html>
"""

addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket()
s.bind(addr)
s.listen(1)

print('listening on', addr)

#Temperature code part
i2c = machine.I2C(scl=machine.Pin(22), sda=machine.Pin(23))

address = 24
temp_reg = 5
res_reg = 8

data = bytearray(2)

#Infinite loop
while True:
    data = i2c.readfrom_mem(address, temp_reg, 2)
    
    cl, addr = s.accept()
    print('client connected from', addr)
    cl_file = cl.makefile('rwb', 0)
    while True:
        line = cl_file.readline()
        decoded = line.decode()
        if "GET" in decoded:
            path = decoded.split(" ")[1]
        #print(line)
        if not line or line == b'\r\n':
            break
    rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    temp = '<h2 class="title">Temperature data: <span id="temp">%s</span> Celsius</h2>' % str(temp_cal.temp_c(data))
    response = (html % '\n'.join(rows)) + temp
    if path == "/sensors":
        response = h200_json+json.dumps(temp_cal.temp_c(data))
    if path == "/pins":
        response = h200_json+json.dumps([str(i) for i in pins])
    if path.startswith("/neopixel/"):
        neo_data = path.split("/")
        if len(neo_data) < 3:
            print("Error with the neopixel request")
        
        if neo_data[2] == "get": 
            response = h200_json+json.dumps([[i[1],i[0],i[2]] for i in np])
        if neo_data[2] == "off":
            for i in range(3):
                np[i] = (0,0,0)
            np.write()
            response = h200_json
        if neo_data[2] == "change":
            colors = neo_data[4].split(",")
            np[int(neo_data[3])] = (int(colors[1]),int(colors[0]),int(colors[2]))
            np.write()
            response = h200_json
        
    cl.send(response)
    cl.close()


                #console.log(document.getElementById("pins").rows[0].text);
                #console.log(document.getElementById("pins").rows[2].text);